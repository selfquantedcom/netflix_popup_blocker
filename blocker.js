
try{
   chrome.storage.sync.get('activated', function(data) {
      var activated = data.activated;
      console.log(activated);
      if(activated != "deactivated"){         
         clearInterval(window.netflixPopupBlocker);         
         window.netflixPopupBlocker = setInterval(function(){  
            executeBlocker();
         }, 1000);      
      }      
   })
   
} catch {

}

function executeBlocker(){ 
   var rows = document.querySelectorAll('.rowContent');
   var items = document.querySelectorAll('.slider-item');
   for (var i = 0; i < rows.length; i++) {
      for (var j = 0; j < items.length; j++) {      
         var target = rows[i].querySelector(`.slider-item-${j}`);
         if(rows[i].querySelector(`.slider-item-${j} a`)) {
            var href = rows[i].querySelector(`.slider-item-${j} a`).href;            
            target.setAttribute('onclick', `window.open("${href}", "_self")`);
            target.setAttribute('style', 'cursor: pointer');
            target.querySelector('.title-card-container').setAttribute('style', 'pointer-events: none;');
         } else {
            break
         }     
      }
   }
}


