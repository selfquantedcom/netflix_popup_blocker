var input = document.querySelector('#plugin-activation');

// Prefill input
chrome.storage.sync.get('activated', function(data) {
   var activated = data.activated;
   console.log(activated);
   if(activated == "deactivated"){         
      input.checked = false
   }      
})

input.onchange = function(event){
   var activated = event.target.checked;
   if (activated){
      chrome.storage.sync.set({activated: "activated"});
   } else {
      chrome.storage.sync.set({activated: "deactivated"});
   }
   chrome.tabs.query({active: true, currentWindow: true}, function(tabs) {
      chrome.tabs.executeScript(
         tabs[0].id,
         {code: `          
            function executeBlocker(){
               console.log('executing blocker'); 
               var rows = document.querySelectorAll('.rowContent');
               var items = document.querySelectorAll('.slider-item');
               for (var i = 0; i < rows.length; i++) {
                  for (var j = 0; j < items.length; j++) {      
                     var target = rows[i].querySelector('.slider-item-' + j);
                     console.log('target:' + target); 
                     if(rows[i].querySelector('.slider-item-' + j + ' a')) {
                        var href = rows[i].querySelector('.slider-item-' + j + ' a').href; 
                        console.log('href:' + href);            
                        target.setAttribute('onclick', 'window.open("' + href + '" , "_self")');
                        target.setAttribute('style', 'cursor: pointer');
                        target.querySelector('.title-card-container').setAttribute('style', 'pointer-events: none;');
                     } else {
                        break
                     }     
                  }
               }
            }                     
            clearInterval(window.netflixPopupBlocker);  
            console.log('${activated}');
            if(${activated}){
               window.netflixPopupBlocker = setInterval(function(){  
                  executeBlocker();
               }, 1000);
            }            
            `
         });
   });
};





